## XvT (X-Wing vs. TIE Fighter) README/To-Do

Welcome to the readme. Not much here yet.

<br>

### Planned components:

* Stimulus generator. Doing this in Matlab b/c that's easy.
  * Done? At least for a first version

* Actual webpage. Will plan to use MJ's old M-Turk code as a starter point.
  * xvt1 now done and running


### Stimuli:

xvt1: using stimuli from `xvt_stims_5_20_70_40_black` 20°-160° inclusive, 10° increments

<br>

### XvT1 to-do:

* Verify timing/payment/instructions
  * Done, although could always be checked closer...

* Write JS task instructions (`do_instructions12()` onward)
  * Done, although could always be checked closer/improved...

* Debug actual task
  * Done (ditto above, always could be done more/better)

* Change `debug` back to `active` in filenames when ready to go
  * Done

* Consult my old to-do checklists for previous M-Turk studies
  * PDF instructions, how to make a HIT, etc.
  * Done


### Notes to self from JS doc:

```
//notes to self
// probably change cursor back during breaks -- Done, I think?
// note that if we do need to change colors of things, there was some of that in VSTM do_trials3() -- looks like we aren't doing any of that now
```

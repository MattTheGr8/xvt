function xvt_stimgen( line_radius, cockpit_radius, wing_length, wingtip_length, fill_color )
% all measurements in pixels, should be integers
% assumptions: wing is significantly longer than wingtip and cockpit, wingtip bigger than cockpit, line radius is small compared to other values
%            - (basically, this thing will crash if you give it weird proportions)
% fill_color should be 0-1 range, I guess
% future: maybe add anti-aliasing/blurring
%       - to make that easier, use alpha rather than "transparency" when saving PNGs

% other notes:
% - this is totally gonna be inefficient but I do not care
% - also just gonna use the same filenames always, at least for now
%   - if we want multiple sets, can just drop 'em in folders
% - similarly, always going to use 1-degree increments for wingtip rotation
%   - nothing stopping us from using just a subset of stimuli in the experiment
% - degrees in filenames will refer to the angle of the upper right wingtip



pic_width  = 1 + 2*wing_length + 2*wingtip_length + 2*line_radius;
pic_height = 1 +                 2*wingtip_length + 2*line_radius;

x_center = ceil( pic_width/2);
y_center = ceil(pic_height/2);

stim_mask = zeros( pic_height, pic_width );

%draw cockpit
stim_mask = xvt_fill_circle( stim_mask, x_center, y_center, cockpit_radius );

%draw wings
for i = 0:wing_length
    stim_mask = xvt_fill_circle( stim_mask, x_center + i, y_center, line_radius );
    stim_mask = xvt_fill_circle( stim_mask, x_center - i, y_center, line_radius );
end

stim_mask_no_wingtips = stim_mask;

for ang = 0:180
    stim_mask = stim_mask_no_wingtips;
    
    for i = 0:wingtip_length
        this_x_offset = i * cosd(ang);
        this_y_offset = i * sind(ang);
        
        stim_mask = xvt_fill_circle( stim_mask, x_center + wing_length + this_x_offset, y_center + this_y_offset, line_radius );
        stim_mask = xvt_fill_circle( stim_mask, x_center + wing_length + this_x_offset, y_center - this_y_offset, line_radius );
        
        stim_mask = xvt_fill_circle( stim_mask, x_center - wing_length - this_x_offset, y_center + this_y_offset, line_radius );
        stim_mask = xvt_fill_circle( stim_mask, x_center - wing_length - this_x_offset, y_center - this_y_offset, line_radius );
    end
    
    out_img(:,:,1) = fill_color(1) * stim_mask;
    out_img(:,:,2) = fill_color(2) * stim_mask;
    out_img(:,:,3) = fill_color(3) * stim_mask;
    alpha          = stim_mask;
    
    out_fname = sprintf('xvt_ang_%.3d.png', ang);
    imwrite(out_img,out_fname,'alpha',alpha);
end




function mask = xvt_fill_circle( mask, x, y, radius )

xs = 1:size(mask,2);
ys = 1:size(mask,1);

[xs, ys] = meshgrid( xs, ys );

dists2 = (xs - x).^2 + (ys - y).^2;

fill_inds = dists2 <= (radius .^ 2);

mask(fill_inds) = 1;
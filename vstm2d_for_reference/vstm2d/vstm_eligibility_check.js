// General global variables
var cgibin_dir;                // will get prepended to Python script names; leave as empty string if no such dir
                               //  n.b., if it does exist, remember to end with trailing slash!
//cgibin_dir = "/cgi-bin/";    //for localhost testing (if using MAMP or similar)
cgibin_dir = "";               //for Dreamhost server, or MRJ's Linode, etc.
var elig_check_scriptname = "vstm2d_check_workerid.py";

// Identifiers and such that are not super-likely to change except between fairly different experiments
var instructions_id = "#instructions";
var instructions_bg_id = "#instructions_bg";
var next_button_id = "#nextbutton";

// Other global variables that will be used throughout the task
//  but will get declared/initialized here.
var win_width, win_height;
var win_center_x, win_center_y;
var worker_id = "";
var worker_id_valid;
var worker_id_used_before = -1; //-2:error, -1:undefined, 0:unused, 1:used

//$(document).ready(vstm_onready);
$(window).load(vstm_onready); //this is better; only runs once all images are loaded

function vstm_onready()
{
  //some initial task setup code here
  win_width = $(window).width();
  win_height = $(window).height();
  win_center_x = win_width / 2;
  win_center_y = win_height / 2;

  //center instructions
  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );

  do_worker_id_entry();
}

function do_worker_id_entry()
{
  $(instructions_id).html("<b>M-Turk Worker ID Eligibility Check</b><br><br>" +
                          "Please enter your Amazon M-Turk worker ID in the box below to check whether you are eligibible to participate in this HIT. (You are ineligible if you have completed this HIT before, or a similar task in a previous HIT, as specified in the HIT description.)<br><br>" +
                          "Your M-Turk worker ID should be a 12-to-15 character series of random letters and numbers, starting with A (for example, A1BGDXZ95IQ3W).<br><br>" +
                          '<input type="text" id="mturk_worker_id_input"><br><br>' +
                          "(Click the NEXT button to continue.)");

  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 0, 275 );
  $(next_button_id).off("click");
  $(next_button_id).css('cursor','pointer');
  $(next_button_id).click( worker_id_entry_validate_input );
}

function worker_id_entry_validate_input() // valid worker ID entered?
{
  var user_input_worker_id = $("#mturk_worker_id_input").val();

  if (validate_worker_id(user_input_worker_id))
  {
    do_wait_screen();
    worker_id = user_input_worker_id;
    setTimeout( check_worker_id_used_before, 2000 );
  }
  else
  {
    alert("It looks like you put in something that is not a valid format for an M-Turk worker ID! Please try again. If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.");
  }
}

function do_wait_screen()
{
  $(instructions_id).html("<b>Checking now...</b><br><br>" +
                          "Currently checking log files to see if you are eligible.<br><br>" +
                          "This screen should update with a result in no more than a few seconds. If it takes longer, please reload the page and try again.<br><br>" +
                          "If you experience trouble checking your worker ID, please email the experimenter at matthew.r.johnson@unl.edu.");

  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
  $(next_button_id).off("click");
  $(next_button_id).hide();
}

function do_worker_id_used_error()
{
  $(instructions_id).html("<b>Status: Ineligible</b><br><br>" +
                          "Sorry, it looks like your worker ID has already been used to complete this HIT (or a similar task in a previous HIT, as specified in the HIT description). Thus, you are ineligible to do the same experiment again.<br><br>" +
                          "If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.<br><br>" +
                          "Otherwise, please return the HIT.");
  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
}

function do_worker_id_unused_success()
{
  $(instructions_id).html("<b>Status: ELIGIBLE!</b><br><br>" +
                          "Excellent, it looks like you have not previously completed a HIT that would make you ineligible to participate in this experiment.<br><br>" +
                          "Read through the HIT description if you haven't already, and if you are interested in participating, feel free to accept the HIT!");
  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
}

function do_worker_id_check_result()
{
  if (worker_id_used_before == 1)        //they've done this before; crash out
    do_worker_id_used_error();
  else if (worker_id_used_before == 0)   //they are clear to go
    do_worker_id_unused_success();
  else                                   //there was some kind of error; go to error screen
    do_worker_id_check_error();
}

function do_worker_id_check_error()
{
  $(instructions_id).html("<b>Error checking worker ID!</b><br><br>" +
                          "Sorry, it looks like our system encountered a problem while trying to check your M-Turk worker ID. In theory this should never happen, but web servers can be unpredictable sometimes.<br><br>" +
                          "Try reloading this web page (or even close this window/tab and try clicking the original link again). If the problem occurs a second time, please email the experimenter at matthew.r.johnson@unl.edu.");
  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
}

function set_object_center( obj_id, x_coord, y_coord )
// moves object center to (x_coord, y_coord)
//  in coordinate system where (0,0) is window center
// n.b.: can only call after globals win_center_x and win_center_y are set!
{
  var obj_width, obj_height;
  var top_coord, left_coord;

  obj_width = $(obj_id).outerWidth();
  obj_height = $(obj_id).outerHeight();
  top_coord = y_coord - (obj_height/2) + win_center_y;
  left_coord = x_coord - (obj_width/2) + win_center_x;
  $(obj_id).offset({ top: top_coord, left: left_coord});
}

function validate_worker_id( id_str )
{
  var min_length = 10;
  var max_length = 25;
  var bad_reg_ex = /\W/;

  if (id_str.length < min_length)
    return 0;

  if (id_str.length > max_length)
    return 0;

  if (id_str.charAt(0) != 'A' && id_str.charAt(0) != 'a')
    return 0;

  if (bad_reg_ex.test(id_str))
    return 0;

  return 1;
}

function check_worker_id_used_before()
{
  $.post( cgibin_dir + elig_check_scriptname, { workerid: worker_id }, check_worker_id_callback );
}

function check_worker_id_callback( check_status )
{
  if (check_status == "used")
    worker_id_used_before = 1;
  else if (check_status == "unused")
    worker_id_used_before = 0;
  else if (check_status == "error")
    worker_id_used_before = -2;
  else
    worker_id_used_before = -1;

  do_worker_id_check_result();
}

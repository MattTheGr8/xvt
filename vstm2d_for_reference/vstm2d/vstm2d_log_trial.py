#!/usr/bin/python

output_fname = '/var/www/mturkstudi.es/data/active/vstm2d_trial_log_{0}.txt' #the placeholder will get filled with the worker ID
owner_name = 'www-data'
group_name = 'datareaders'
python_log_dir = '/var/www/mturkstudi.es/log/pylogs'

import sys, cgi, cgitb, datetime
import os, pwd, grp, stat

cgitb.enable(display=0, logdir=python_log_dir)

formdata = cgi.FieldStorage()

workerid     = formdata.getvalue("workerid",      "WORKERID_NULL")
trialnum     = formdata.getvalue("trialnum",      "TRIAL_NULL")
trialnum_c   = formdata.getvalue("trialnum_c",    "TRIAL_C_NULL" )
acc_feedback = formdata.getvalue("acc_feedback",  "ACC_FB_NULL")
acc_closest  = formdata.getvalue("acc_closest",   "ACC_CLOSE_NULL")
resp_time    = formdata.getvalue("resp_time",     "RESP_TIME_NULL")
hemifield    = formdata.getvalue("hemifield",     "HEMIFIELD_NULL")
eccen        = formdata.getvalue("eccen",         "ECCEN_NULL")
probe_ind    = formdata.getvalue("probe_ind",     "PROBE_IND_NULL")
click_x      = formdata.getvalue("click_x",       "CLICK_X_NULL")
click_y      = formdata.getvalue("click_y",       "CLICK_Y_NULL")
targ1_deg    = formdata.getvalue("targ1_deg",     "TARG1_DEG_NULL")
targ2_deg    = formdata.getvalue("targ2_deg",     "TARG2_DEG_NULL")
targ1_x      = formdata.getvalue("targ1_x",       "TARG1_X_NULL")
targ2_x      = formdata.getvalue("targ2_x",       "TARG2_X_NULL")
targ1_y      = formdata.getvalue("targ1_y",       "TARG1_Y_NULL")
targ2_y      = formdata.getvalue("targ2_y",       "TARG2_Y_NULL")
targ1_hue    = formdata.getvalue("targ1_hue",     "TARG1_HUE_NULL")
targ2_hue    = formdata.getvalue("targ2_hue",     "TARG2_HUE_NULL")
win_resized  = formdata.getvalue("win_resized",   "WIN_RESIZE_NULL")

output_fname = output_fname.format(workerid);
timestamp_str = '{0}'.format( datetime.datetime.utcnow() )
filehandle = open(output_fname, 'a')
to_print = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\n'.format(
           workerid, trialnum, trialnum_c, acc_feedback, acc_closest, resp_time, hemifield, eccen, probe_ind, click_x, click_y,
           targ1_deg, targ2_deg, targ1_x, targ2_x, targ1_y, targ2_y, targ1_hue, targ2_hue, win_resized, timestamp_str )
filehandle.write( to_print )
filehandle.close()

#new -- try fixing permissions/ownership up real nice
uid = pwd.getpwnam(owner_name).pw_uid
gid = grp.getgrnam(group_name).gr_gid
os.chown(output_fname, uid, gid)
os.chmod(output_fname, stat.S_IWUSR | stat.S_IRUSR | stat.S_IRGRP )

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')
sys.stdout.write('Done.')

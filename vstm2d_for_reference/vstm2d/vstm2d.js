/* global $ */
// General task parameter global variables
var n_targets = 2;
var lab_center = [70, 20, 38];
var lab_radius = 60;
var initial_fixation = 5000;
var target_display_time = 1000;
var delay_time = 1000;
var n_trials = 50;
var iti = 2000;
var midline_buffer = 15; //in degrees; can't get closer to midline than this
var target_buffer = 10; //in degrees; targets can't get closer together than this (degrees on circle, not visual angle, btw)
var eccentricity_min = 200;  // in pixels
var eccentricity_max = 200;  // in pixels
var feedback_correct_id = "#smiley";
var feedback_incorrect_id = "#frowny";
var feedback_time = 1000;
var max_error_for_correct = 50; // in pixels
var require_closest_for_feedback = 0;
var require_correct_trials = 1; // do we just do n_trials trials (0) or n_trials CORRECT trials? (1)
var break_every_n_trials = 10; // set to 0 for no breaks; will refer to require_correct_trials above as appropriate
var cgibin_dir;                // will get prepended to Python script names; leave as empty string if no such dir
                               //  n.b., if it does exist, remember to end with trailing slash!
//cgibin_dir = "/cgi-bin/";    //for localhost testing
cgibin_dir = "";               //for Dreamhost server, or MRJ's Linode, etc.

// Identifiers and such that are not super-likely to change except between fairly different experiments
var target_id_prefix = "#target";
var fixation_id = "#fixpoint";
var break_text_id = "#break";
var break_text_2_id = "#break2";
var instructions_id = "#instructions";
var instructions_bg_id = "#instructions_bg";
var next_button_id = "#nextbutton";
var back_button_id = "#backbutton";
var demographics_div_id = "#demographics_survey_div";
var window_resized_error_id = "#winresized";
var mobile_browser_error_id = "#mobilebrowser";
// n.b. some other element IDs are hard-coded in functions below, but generally only in single-purpose
//  functions where the relevant elements should be fairly obvious

// Other global variables that will be used throughout the task
//  but will get declared/initialized here.
var current_trial = 1; // this one increments after each trial, regardless of correctness
var current_trial_correctonly = 1; // this one only increments after correct trials
var probe_index = 0;
var targets_showing = 1;
//var hue_max = 360 / n_targets;
//var hue_max = 120; // fix to be the same as vstm2b, even though we could have targets 180 degrees apart in hue

var firsthue_max = 240; // above fix didn't quite work; old code assumed hues would be evenly spaced. So now we
var hue_separation = 120; // have to explicitly specify how far apart the two targets' hues are, separately
                          // from the range of the first target's hue

var first_target_hue;
var target_hues = [];
var win_width, win_height;
var win_resize_trial_invalid = 0;
var win_center_x, win_center_y;
var target_dot_degrees = [];
var this_hemifield;
var this_ecc;
var target_xs = [];
var target_ys = [];
var click_x, click_y;
var this_closest_accuracy;
var this_feedback_accuracy;
var cursor_showing = 1;
var probe_onset_time;
var this_rt;
var worker_id = "";
var worker_id_valid;
var worker_id_used_before = -1; //-2:error, -1:undefined, 0:unused, 1:used
var user_agent_string = navigator.userAgent;
var probe_id;

//$(document).ready(vstm_onready);
$(window).load(vstm_onready); //this is better; only runs once all images are loaded

function vstm_onready()
{
  //some initial task setup code here
  hide_targets();
  win_width = $(window).width();
  win_height = $(window).height();
  win_center_x = win_width / 2;
  win_center_y = win_height / 2;
  // (when all is said and done, remember to make sure win_width and win_height are up to date)

  //hide feedback images
  $(feedback_correct_id).hide();
  $(feedback_incorrect_id).hide();

  //hide break text
  $(break_text_id).hide();
  $(break_text_2_id).hide();

  //hide demographic survey
  $(demographics_div_id).hide();
  $("#demographics_race_popup").hide();
  $("#demographics_ethnicity_popup").hide();
  $("#demographics_sex_popup").hide();
  $("#demographics_age_popup").hide();

  //hide fixation until after instructions
  $(fixation_id).hide()

  //center instructions
  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );

  //center window resized error message and hide it
  set_object_center( window_resized_error_id, 0, 0 );
  $(window_resized_error_id).hide();

  //get worker ID if embedded in URL
  worker_id = getParamFromURL( "workerId" );
  worker_id_valid = validate_worker_id( worker_id );
  if (worker_id_valid)
  {
    check_worker_id_used_before( worker_id );
  }

  //check that user is not on a mobile device, and if they're not, begin instructions
  if (validate_browser(user_agent_string))
  {
    $(mobile_browser_error_id).hide();
    do_instructions1();
  }
  else
  {
    $(back_button_id).hide();
    $(next_button_id).hide();
    $(instructions_id).hide();
    $(instructions_bg_id).hide();
    set_object_center( mobile_browser_error_id, 0, 0);
  }
}

function do_instructions1()
{
  $(instructions_id).html("<b>Informed Consent Form</b><br><br>" +
                          "<b>Purpose:</b> We are conducting a research study to examine the factors that affect cognition, or your ability to think. You are invited to participate in this study if you are a healthy adult aged 17 or older.<br><br>" +
                          "<b>Procedures:</b> Participation in this study will involve seeing words, numbers, or pictures and/or thinking about words, numbers, or pictures that are no longer present. You will also be asked to complete a demographics questionnaire. You will get specific instructions about how to do the task before it begins. Participants will receive $0.75 per 15 minutes or part thereof. We anticipate that your involvement in this study will require approximately 20-30 minutes.<br><br>" +
                          "(Click the NEXT button to continue.)");

  set_object_center( instructions_bg_id, 0, 0 );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 0, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(back_button_id).css('cursor','pointer');
  $(next_button_id).css('cursor','pointer');
  $(back_button_id).hide();
  $(next_button_id).click( do_instructions2 );
}

function do_instructions2()
{
  $(instructions_id).html("<b>Informed Consent Form</b><br><br>" +
                          "<b>Risks and Benefits:</b> Completing these study questionnaires and tasks creates no more risk of harm to you than do the experiences of everyday life (e.g., from working on a computer). Although this study will not benefit you personally, it will contribute to the advancement of our understanding of human memory.<br><br>" +
                          "<b>Confidentiality:</b> All of the responses you provide during this study will be anonymous. You will not be asked to provide any identifying information, such as your name, in any of the questionnaires. Typically, only the researchers involved in this study and those responsible for research oversight will have access to the information you provide. However, we may also share the data with other researchers so that they can check the accuracy of our conclusions; this will not impact you because the data are anonymous.<br><br>" +
                          "(Click the NEXT button to continue.)");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  $(back_button_id).show();
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions3 );
  $(back_button_id).click( do_instructions1 );
}

function do_instructions3()
{
  $(instructions_id).html("<b>Informed Consent Form</b><br><br>" +
                          "<b>Confidentiality (continued):</b> This study is run through the Amazon Mechanical Turk platform. Amazon is a private company not affiliated with the research team. The researchers will identify you only by your Mechanical Turk worker ID (for example, A123456789). Any other information you share with Amazon is not recorded or stored by the research team, and is covered by M-Turk's <a href=https://www.mturk.com/mturk/privacynotice>privacy policy</a>.<br><br>" +
                          "<b>Questions:</b> If you have any questions about this study, you may contact the principal investigator, Matthew Johnson, at matthew.r.johnson@unl.edu or (402) 472-3606. (For technical issues, please email matthew.r.johnson@unl.edu.)<br><br>" +
                          "(Click the NEXT button to continue.)");

  $(next_button_id).text( "NEXT" );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions4 );
  $(back_button_id).click( do_instructions2 );
}

function do_instructions4()
{
  $(demographics_div_id).hide(); //in case they hit "back" from the demographics form
  $(instructions_id).html("<b>Informed Consent Form</b><br><br>" +
                          "<b>Agreement to participate:</b> By checking the checkbox and then clicking the \"Consent/Next\" button below, you acknowledge that you have read the above information, and agree to participate in the study.<br><br>" +
                          "You must be at least 17 years of age to participate; agreeing to participate confirms you are 17 years of age or older.<br><br>" +
                          '<input type=checkbox name="cnst_ckbox" id="consent_checkbox"> I confirm these statements and agree to participate.<br><br>' +
                          "(Click the CONSENT/NEXT button to confirm your agreement and continue.)");

  $(next_button_id).text( "CONSENT/NEXT" );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( check_consent_checkbox );
  $(back_button_id).click( do_instructions3 );
}

function check_consent_checkbox()
// a tad hacky -- should really define checkbox ID up at the top of the script...
{
  if (document.getElementById("consent_checkbox").checked)
    do_instructions5();
  else
    window.alert("You must check the box to confirm your agreement to participate.\n\nIf you do not (or cannot) consent to participating, please close this browser window and return the HIT.");
}

function do_instructions5()
// do demographics questionnaire here, also get MTurk ID if we couldn't get it from the URL
{
  if (worker_id_used_before == 1)        //they've done this before; crash out
    do_worker_id_used_error();
  else if (worker_id_used_before == 0)   //they are clear to go; skip worker ID manual entry
    do_instructions5b();
  else                                   //couldn't get worker ID from URL (either not specified, or there was an error; either way, go to manual entry)
    do_instructions5a();
}

function do_instructions5a() // manual entry of MTurk worker ID
{
  $(instructions_id).html("<b>M-Turk Worker ID Entry</b><br><br>" +
                          "It looks like we were unable to automatically determine your Amazon M-Turk worker ID. Please enter it in the box below.<br><br>" +
                          "Your M-Turk worker ID should be a 12-to-15 character series of random letters and numbers, starting with A (for example, A1BGDXZ95IQ3W).<br><br>" +
                          '<input type="text" id="mturk_worker_id_input"><br><br>' +
                          "(Click the NEXT button to continue.)");

  $(next_button_id).text( "NEXT" );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions5a_validate_input );
  $(back_button_id).click( do_instructions4 );
}

function do_instructions5a_validate_input() // valid worker ID entered?
{
  var user_input_worker_id = $("#mturk_worker_id_input").val();

  if (validate_worker_id(user_input_worker_id))
  {
    check_worker_id_used_before(user_input_worker_id);
    worker_id = user_input_worker_id;
    do_instructions5b();
  }
  else
  {
    alert("It looks like you put in something that is not a valid format for an M-Turk worker ID! Please try again. If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.");
  }
}

function do_instructions5b()
{
  //clear instructions div for now, actually show pre-fab demographics form
  $(instructions_id).html("");
  $(demographics_div_id).show();
  attach_popup("#race_selector","#demographics_race_popup");
  attach_popup("#ethnicity_selector","#demographics_ethnicity_popup");
  attach_popup("#gender_selector","#demographics_sex_popup");
  attach_popup("#age_selector","#demographics_age_popup");

  $(next_button_id).text( "NEXT" );
  set_object_center( instructions_id, 0, 0 );
  set_object_center( demographics_div_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( validate_demographics );
  $(back_button_id).click( do_instructions4 );
}

function attach_popup( item_id, popup_id )
{
  $(item_id).mouseenter( {popup_id: popup_id}, activate_popup );
  $(item_id).mouseout( {popup_id: popup_id}, deactivate_popup );
}

function activate_popup( e )
{
  $(e.data.popup_id).show();
  set_object_center( e.data.popup_id, 300, e.pageY - win_center_y );
}

function deactivate_popup( e )
{
  $(e.data.popup_id).hide();
}

function do_worker_id_used_error()
{
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).hide();
  $(back_button_id).hide();
  $(instructions_id).html("<b>You have already completed this HIT!</b><br><br>" +
                          "Sorry, it looks like your worker ID has already been used to complete this HIT (or a similar task in a previous HIT, as specified in the HIT description). Thus, you are ineligible to do the same experiment again.<br><br>" +
                          "If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.<br><br>" +
                          "Otherwise, please return the HIT.");
  set_object_center( instructions_id, 0, 0 );
}

function validate_demographics()
{
  //going to just hard-code object IDs here, so sue me...
  var race_val =       $("#race_selector").val();
  var ethnicity_val =  $("#ethnicity_selector").val();
  var gender_val =     $("#gender_selector").val();
  var age_val =        $("#age_selector").val();

  if (race_val=="RaceEmpty" || ethnicity_val=="EthnicityEmpty" || gender_val=="SexEmpty" || age_val=="AgeEmpty")
  {
    alert('None of the values in the demographics form should be left empty! Please try again. If you prefer not to provide your race, ethnicity, or gender, please select "Prefer not to answer." If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.');
  }
  else
  {
    $.post( cgibin_dir + "vstm2d_log_demographics.py", { workerid: worker_id,
                                                        race: race_val, ethnicity: ethnicity_val,
                                                        gender: gender_val, age: age_val, uas: user_agent_string } );
    do_instructions6();
  }
}

function do_instructions6()
{
  $(demographics_div_id).hide();
  if (worker_id_used_before == 1)        //they've done this before; crash out
    do_worker_id_used_error();
  else if (worker_id_used_before == 0)   //they are clear to go
    do_instructions6a();
  else                                   //there was some kind of error; go to error screen
    do_worker_id_logging_error();
}

function do_worker_id_logging_error()
{
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).hide();
  $(back_button_id).hide();
  $(instructions_id).html("<b>Error processing worker ID!</b><br><br>" +
                          "Sorry, it looks like our system encountered a problem while trying to log your M-Turk worker ID. In theory this should never happen, but web servers can be unpredictable sometimes.<br><br>" +
                          "Please close this window/tab and try reloading the HIT from the original link. If the problem occurs a second time, please email the experimenter at matthew.r.johnson@unl.edu.");
  set_object_center( instructions_id, 0, 0 );
}


function do_instructions6a()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Please maximize your browser window at this time so that it fills your entire screen.<br><br>" +
                          "If you have multiple monitors, please position yourself such that one monitor is directly in front of you, and maximize the browser window so that it fills that display.<br><br>" +
                          "Once your browser window is maximized and filling the screen, you may continue.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions7 );
  $(back_button_id).click( do_instructions5 );
}

function do_instructions7()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Once maximized, your browser window should be large enough to view the entire dark gray square containing this text. (This should be true on almost all modern computer displays.)<br><br>" +
                          "If your display is not large enough, unfortunately you will not be able to complete this HIT. If that is the case, please try again later on a larger display!");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions8 );
  $(back_button_id).click( do_instructions6 );
}

function do_instructions8()
// do window size check here -- remind not to make smaller
{
  win_width = $(window).width();
  win_height = $(window).height();
  win_center_x = win_width / 2;
  win_center_y = win_height / 2;

  $(next_button_id).off("click");
  $(back_button_id).off("click");
  if (win_width < $(instructions_bg_id).width() || win_height < $(instructions_bg_id).height()) //window too small
  {
    $(instructions_id).html("<b>Window too small!</b><br><br>" +
                            "Sorry, it looks like your browser window is too small to adequately display the experimental task. Please try again on a larger display.<br><br>" +
                            "If you are unable to perform the task on a larger display, please return the HIT.<br><br>" +
                            "If you believe you have reached this message in error, please email the experimenter at matthew.r.johnson@unl.edu.");
    $(next_button_id).hide();
    $(back_button_id).hide();
    set_object_center( instructions_id, 0, 0 );
  }
  else //window size OK; proceed
  {
    $(instructions_id).html("<b>Window size check: OK</b><br><br>" +
                            "Excellent! It looks like your browser window is large enough to display the experimental task.");
    $(window).resize( window_was_resized );
    set_object_center( instructions_bg_id, 0, 0 );
    set_object_center( instructions_id, 0, 0 );
    set_object_center( next_button_id, 100, 275 );
    set_object_center( back_button_id, -100, 275 );
    $(next_button_id).click( do_instructions9 );
    $(back_button_id).click( do_instructions7 );
  }
}

function do_instructions9()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "It is very important that you do not resize your browser window while you are performing the experimental task. During breaks, you may switch to other applications or tabs, but take care not to resize the browser window.<br><br>" +
                          "<b>IF YOU RESIZE YOUR BROWSER WINDOW, THE EXPERIMENTAL TASK WILL BE INTERRUPTED AND YOU WILL NOT BE ABLE TO COMPLETE THE HIT!!!</b>");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions10 );
  $(back_button_id).click( do_instructions8 );
}

function do_instructions10()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Please position yourself at a reasonable viewing distance from the screen. If you are using a laptop or a desktop computer with a mid-size monitor, your typical viewing distance (about an arm's length away) should be fine.<br><br>" +
                          "If you are using a larger monitor, hold your hand out in front of you with your arm fully extended, your fingers together, and your palm facing the screen. At a good viewing distance, the dark gray square containing this text should appear to be about the same width as your hand (or slightly larger if you have small hands).");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions11 );
  $(back_button_id).click( do_instructions9 );
}


function do_instructions11()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Now, here are the instructions for the experimental task. Please make sure you read and understand them completely before continuing.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions12 );
  $(back_button_id).click( do_instructions10 );
}

function do_instructions12()
{
  var require_correct_string;
  if (require_correct_trials)
    require_correct_string = " CORRECTLY ";
  else
    require_correct_string = " ";

  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "In this study, you will perform the same task a number of times (trials). On each trial, you will be shown several objects on the screen and will have to remember their locations. Then, you will be asked to indicate where one of the objects appeared, to test your memory.<br><br>" +
                          "The task is not too difficult, but it will require you to pay close attention at all times.<br><br>" +
                          "You will need to complete " + n_trials + " trials" + require_correct_string + "to get credit for the HIT. These must be completed in a single session, although you will get the opportunity to take a short break every few trials if you need to.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions13 );
  $(back_button_id).click( do_instructions11 );
}

function do_instructions13()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Breaks are not timed, but please try to keep them brief (3 minutes or so at the most) so that you don't lose focus on the experimental task.<br><br>" +
                          "Again, you may feel free to switch tabs or windows during breaks, but remember not to resize this browser window, or you will interrupt the experiment and be unable to complete the HIT.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions14 );
  $(back_button_id).click( do_instructions12 );
}

function do_instructions14()
{
  //$(instructions_id).html("<b>Instructions</b><br><br>" +
  //                        "When the task begins, you will first see a small dot in the center of the screen. It is important that you <b>keep your eyes fixed right on this dot</b> -- don't move them around the screen.<br><br>" +
  //                        "After a few seconds, the next thing you will see is three larger colored circles appearing at various locations around the screen. Your task is to <b>remember, as accurately as possible, the locations and colors of those circles</b>.");
  // change for fixation-recolor probe (cross)
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "When the task begins, you will first see a small cross in the center of the screen. It is important that you <b>keep your eyes fixed right on this cross</b> -- don't move them around the screen.<br><br>" +
                          "After a few seconds, the next thing you will see is two larger colored circles appearing at various locations around the screen. Your task is to <b>remember, as accurately as possible, the locations and colors of those circles</b>.");


  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions15 );
  $(back_button_id).click( do_instructions13 );
}

function do_instructions15()
{
  //$(instructions_id).html("<b>Instructions</b><br><br>" +
  //                        "Note that the colored circles will not be onscreen very long, so you must <b>keep your attention very focused</b> in order to remember them accurately.<br><br>" +
  //                        "Also note that the small central dot will remain onscreen when those colored circles appear. <b>Resist the temptation to move your eyes when the colored circles appear</b>; pay attention to them and remember their locations, but continue to keep your eyes focused on the central dot. Your responses will only be useful to us if you can do that.");
  // change for fixation-recolor probe (cross)
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Note that the colored circles will not be onscreen very long, so you must <b>keep your attention very focused</b> in order to remember them accurately.<br><br>" +
                          "Also note that the small central cross will remain onscreen when those colored circles appear. <b>Resist the temptation to move your eyes when the colored circles appear</b>; pay attention to them and remember their locations, but continue to keep your eyes focused on the central cross. Your responses will only be useful to us if you can do that.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions16 );
  $(back_button_id).click( do_instructions14 );
}

function do_instructions16()
{
  //$(instructions_id).html("<b>Instructions</b><br><br>" +
  //                        "Soon after the colored circles disappear, one of them will reappear in the center of the screen. (At this point, you are free to move your eyes again.)<br><br>" +
  //                        "Your job is to <b>recall the original location of the circle shown, and click on that location with your mouse pointer</b>.");
  // change for fixation-recolor probe (cross)
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "Soon after the colored circles disappear, the cross in the center of the screen will change color -- to the color of one of the circles you just saw. (At this point, you are free to move your eyes again.)<br><br>" +
                          "Your job is to <b>recall the original location of the circle that was shown in that color, and click on that location with your mouse pointer</b>.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions17 );
  $(back_button_id).click( do_instructions15 );
}

function do_instructions17()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "After you make your click response, you will get some feedback:<br><br>" +
                          "If your click is close enough to the original location of the indicated circle, you will see a <b>smiley face</b> image appear (indicating a correct response).<br><br>" +
                          "If your click is too far away or if you click the original location of the wrong circle, you will see a <b>frowny face</b> image appear (indicating an incorrect response).");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions18 );
  $(back_button_id).click( do_instructions16 );
}

function do_instructions18()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "You will probably not get 100% of the answers correct, but try to do the best you can!<br><br>" +
                          "The task does require your click to be fairly close to the right location to be counted as correct, so make sure to pay close attention and click carefully.<br><br>" +
                          "(And if you aren't sure about the correct location, just take your best guess and try to pay closer attention on the next trial.)");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions19 );
  $(back_button_id).click( do_instructions17 );
}

function do_instructions19()
{
  //$(instructions_id).html("<b>Instructions</b><br><br>" +
  //                        "The feedback will only last a moment, and then the next trial will begin.<br><br>" +
  //                        "So when the smiley face (or frowny face) disappears and the small dot reappears at the center of the screen, fix your eyes on it and get ready to remember the next set of circles.<br><br>" +
  //                        "Every few trials, you will get to take a short break. When you finish all trials, you will get a completion code for the HIT and a full explanation of what the experiment was about.");
  // change for fixation-recolor probe (cross)
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "The feedback will only last a moment, and then the next trial will begin.<br><br>" +
                          "So when the smiley face (or frowny face) disappears and the small cross reappears at the center of the screen, fix your eyes on it and get ready to remember the next set of circles.<br><br>" +
                          "Every few trials, you will get to take a short break. When you finish all trials, you will get a completion code for the HIT and a full explanation of what the experiment was about.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions20 );
  $(back_button_id).click( do_instructions18 );
}

function do_instructions20()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "This concludes the instructions. When you click the NEXT button, the task will begin.<br><br>" +
                          "<b>YOU WILL NOT BE ABLE TO REVIEW THESE INSTRUCTIONS AFTER THE TASK BEGINS, SO IF ANYTHING IS UNCLEAR, PLEASE CLICK 'BACK' NOW AND REVIEW THEM!</b><br><br>" +
                          "(If you are unable to perform the task and need to see the instructions again, you can always reload the page... but you will have to start the HIT over from the beginning.)");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( do_instructions21 );
  $(back_button_id).click( do_instructions19 );
}

function do_instructions21()
{
  $(instructions_id).html("<b>Instructions</b><br><br>" +
                          "<b>Are you SURE you understand all instructions and are ready to begin the task?</b><br><br>" +
                          "This is really your true final warning before the task begins. Last chance to go back and review instructions!<br><br>" +
                          "If you are <b>100% certain</b> you understand the instructions, click NEXT. The task will begin right after you click.");

  set_object_center( instructions_id, 0, 0 );
  set_object_center( next_button_id, 100, 275 );
  set_object_center( back_button_id, -100, 275 );
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).click( end_instructions );
  $(back_button_id).click( do_instructions20 );
}

function end_instructions()
{
  $(instructions_id).hide();
  $(instructions_bg_id).hide();
  $(next_button_id).off("click");
  $(back_button_id).off("click");
  $(next_button_id).hide();
  $(back_button_id).hide();

  //show and center fixation point
  $(fixation_id).show();
  $(fixation_id).css("background-color","hsl(0,0%,75%)");  // change for fixation-recolor probe
  set_object_center( fixation_id, 0, 0 );

  setTimeout( do_trials1, initial_fixation );
  hide_cursor();
}

function do_trials1() //initial trial setup and show targets to memorize
{
  var target_id, lab, rgb, i;
  var target_degrees_offset;

  // initial trial setup code goes here
  probe_index = Math.floor(Math.random() * n_targets);     //figure out what probe will be
  probe_id = target_id_prefix + probe_index;               //ditto for its id name

  // figure out dot colors
  first_target_hue = Math.floor(Math.random() * firsthue_max);
  for (i = 0; i < n_targets; i++)
  {
    target_hues[i] = first_target_hue + (i*hue_separation);
    target_id = target_id_prefix + i;
    lab = lab4angle( target_hues[i] );
    rgb = xyz2rgb( lab2xyz(lab) );
    $(target_id).css("background-color","rgb("+rgb[0]+","+rgb[1]+","+rgb[2]+")");
  }

  // figure out (and go ahead and set) dot locations
  this_ecc = (Math.random() * (eccentricity_max - eccentricity_min)) + eccentricity_min;
  target_dot_degrees = get_2target_dot_degrees_uniform_distance_distribution(); // new in vstm2d
  if (Math.random() < 0.5)
  {
    this_hemifield = "L";
    target_degrees_offset = 90;
  }
  else
  {
    this_hemifield = "R";
    target_degrees_offset = -90;
  }
  for (i = 0; i < n_targets; i++)
  {
    target_dot_degrees[i] = target_dot_degrees[i] + target_degrees_offset;
    target_xs[i] = Math.cos( deg2rad(target_dot_degrees[i]) ) * this_ecc;
    target_ys[i] = Math.sin( deg2rad(target_dot_degrees[i]) ) * this_ecc;
  }
  show_targets();
  for (i = 0; i < n_targets; i++)
  {
    target_id = target_id_prefix + i;
    set_object_center( target_id, target_xs[i], target_ys[i] ); // get around fact that objects must be visible to set offset?
  }

  setTimeout( do_trials2, target_display_time );
}

function do_trials2() //hide targets for delay period
{
  hide_targets();
  setTimeout( do_trials3, delay_time );
}

function do_trials3() //show one target (the probe)
{
  //$(probe_id).show();
  //set_object_center( probe_id, 0, 0 );
  // this code paragraph: changes for fixation-recolor probe
  var lab, rgb;
  lab = lab4angle( target_hues[probe_index] );
  rgb = xyz2rgb( lab2xyz(lab) );
  $(fixation_id).css("background-color","rgb("+rgb[0]+","+rgb[1]+","+rgb[2]+")");

  probe_onset_time = $.now();
  show_cursor();

  $(document).click( register_probe_click );
}

function do_trials4() //take feedback offscreen, update trial count, set up ITI if there are more trials left
{
  var task_over = 0;
  var need_to_break = 0;

  if (this_feedback_accuracy)
    $(feedback_correct_id).hide();
  else
    $(feedback_incorrect_id).hide();
  $(fixation_id).css("background-color","hsl(0,0%,75%)");  // change for fixation-recolor probe

  //use Python CGI script to log trial data
  $.post( cgibin_dir + "vstm2d_log_trial.py", { workerid: worker_id, trialnum: current_trial, trialnum_c: current_trial_correctonly,
                                               acc_feedback: this_feedback_accuracy, acc_closest: this_closest_accuracy,
                                               resp_time: this_rt, hemifield: this_hemifield, eccen: this_ecc, probe_ind: probe_index,
                                               click_x: click_x, click_y: click_y, targ1_deg: target_dot_degrees[0],
                                               targ2_deg: target_dot_degrees[1], targ1_x: target_xs[0], targ2_x: target_xs[1],
                                               targ1_y: target_ys[0], targ2_y: target_ys[1], targ1_hue: target_hues[0],
                                               targ2_hue: target_hues[1], win_resized: win_resize_trial_invalid } );

  if ( (require_correct_trials && current_trial_correctonly>=n_trials && this_feedback_accuracy) || (!require_correct_trials && current_trial>=n_trials) )
    task_over = 1;

  if (win_resize_trial_invalid)
    task_over = 1;

  if (!task_over)
  {
    //calculate if we have to do a break
    if (break_every_n_trials>0)
    {
      if (require_correct_trials && !(current_trial_correctonly % break_every_n_trials) && this_feedback_accuracy)
        need_to_break = 1;
      else if (!require_correct_trials && !(current_trial % break_every_n_trials))
        need_to_break = 1;
    }

    current_trial++;
    if (this_feedback_accuracy)
      current_trial_correctonly++;

    if (need_to_break)
      setTimeout( do_break, 1000 );
    else
      setTimeout( do_trials1, iti );
  }
  else if (win_resize_trial_invalid)
  {
    show_cursor();
    $(fixation_id).hide();
    $(window_resized_error_id).show();
    set_object_center( window_resized_error_id, 0, 0 );
  }
  else
  {
    var completion_code = generate_completion_code();
    show_cursor();
    $(fixation_id).hide();
    $.post( cgibin_dir + "vstm2d_log_workerid_completion.py", { workerid: worker_id, comp_code: completion_code } );
    $(instructions_bg_id).show();
    $(instructions_id).show();
    $(instructions_id).html('<span style="text-align: center; display: table; font-weight: bold; margin: 0 auto; font-size: 20px; color: black">You\'re all done! Completion code:</span><br><br>' +
                            '<span style="text-align: center; display: table; font-weight: bold; margin: 0 auto; font-size: 24px; color: black">' + completion_code + "</span><br><br>" +
                            "Make sure to write down/copy your completion code now, and then click the NEXT button for an explanation of the experiment.");

    $(next_button_id).show();
    $(next_button_id).click( do_debrief );
    set_object_center( instructions_bg_id, 0, 0 );
    set_object_center( instructions_id, 0, 0 );
    set_object_center( next_button_id, 0, 275 );
  }
}

function do_debrief()
{
  $(instructions_id).html("<b>Debriefing</b><br><br>" +
                          "Thank you for participating in our experiment. This was part of a series of studies examining people's visual short-term memory capacity -- how many visual items they can hold in memory at a time, and how accurately they can remember those items.<br><br>" +
                          "In this version, we are examining how the spatial positioning of the items to remember affects your memory of the item you had to recall. Our hypothesis is that having other items nearby might bias your spatial location judgment for one item either toward or away from other items that were nearby, depending on their specific positioning.<br><br>" +
                          "<b>Please do not tell other M-Turkers about the nature/hypotheses of this study</b> -- we don't want prior knowledge to affect how people approach the task. If you have questions about the study or had any technical difficulties, please contact the experimenter at matthew.r.johnson@unl.edu. Thanks again!");

  set_object_center( instructions_id, 0, 0 );
  $(next_button_id).off("click");
  $(next_button_id).hide();
}

function generate_completion_code()
{
  var n_characters = 20;
  var code_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var comp_code = "";
  var char_ind;

  for (var i = 0; i < n_characters; i++)
  {
    char_ind = Math.floor(Math.random() * code_alphabet.length);
    comp_code = comp_code + code_alphabet.charAt(char_ind);
  }
  return comp_code;
}

function do_break()
{
  var trials_completed;

  if (require_correct_trials)
    trials_completed = current_trial_correctonly - 1;
  else
    trials_completed = current_trial - 1;

  $(break_text_id).show();
  $(break_text_2_id).show();
  $(break_text_2_id).text("Trials completed: " + trials_completed )
  set_object_center( break_text_id, 0, 0 );
  set_object_center( break_text_2_id, 0, 160 );
  show_cursor();
  $(document).click( end_break );
}

function end_break()
{
  $(document).off("click");
  $(break_text_id).hide();
  $(break_text_2_id).hide();
  hide_cursor();
  setTimeout( do_trials1, iti );
}

function register_probe_click( click_event )
// find out where they clicked, figure out accuracy, and display feedback
// n.b.: can only call after globals win_center_x and win_center_y are set!
{
  var this_dist, probe_dist;
  var minimum_dist = 1000000000;
  var click_time = $.now();

  click_x = click_event.pageX - win_center_x;
  click_y = click_event.pageY - win_center_y;
  this_rt = click_time - probe_onset_time;

  $(document).off("click");
  $(probe_id).hide();

  //figure out what they clicked and do feedback
  for (var i = 0; i < n_targets; i++)
  {
    this_dist = Math.sqrt((target_xs[i] - click_x) * (target_xs[i] - click_x) + (target_ys[i] - click_y) * (target_ys[i] - click_y));
    if (this_dist < minimum_dist)
      minimum_dist = this_dist;

    if (i==probe_index)
      probe_dist = this_dist;
  }
  if (probe_dist <= max_error_for_correct && (minimum_dist == probe_dist || !require_closest_for_feedback))
    //correct -- clicked close enough, and either got closest to probe or we said we didn't care about that
  {
    this_feedback_accuracy = 1;
    $(feedback_correct_id).show();
    set_object_center( feedback_correct_id, 0, 0 );
  }
  else
  {
    this_feedback_accuracy = 0;
    $(feedback_incorrect_id).show();
    set_object_center( feedback_incorrect_id, 0, 0 );
  }
  hide_cursor();

  if (minimum_dist == probe_dist)
    this_closest_accuracy = 1;
  else
    this_closest_accuracy = 0;

  setTimeout( do_trials4, feedback_time );
}

// This function has been replaced in 2d with get_2target_dot_degrees_uniform_distance_distribution(), directly below
/*function get_target_dot_degrees()
{
  var targets_good = 0;
  var degrees_min = midline_buffer;
  var degrees_max = 180 - midline_buffer;
  var degrees_range = degrees_max - degrees_min;
  var tdd = [];
  var tdd_sorted = [];
  var i;

  while (!targets_good)
  {
    targets_good = 1;
    for (i = 0; i < n_targets; i++)
      tdd[i] = (Math.random() * degrees_range) + degrees_min;

    tdd_sorted = tdd.slice();
    tdd_sorted.sort( sort_compare_numeric );

    for (i = 0; i < (n_targets-1); i++)
    {
      if ((tdd_sorted[i+1] - tdd_sorted[i]) < target_buffer)
        targets_good = 0;
    }
  }

  return tdd;
}*/

// new in vstm2d; n.b.: ONLY works with 2 targets for now, though should be easy enough to make a 3-target version later
function get_2target_dot_degrees_uniform_distance_distribution()
{
  var degrees_min = midline_buffer;
  var degrees_max = 180 - midline_buffer;
  var degrees_range = degrees_max - degrees_min;

  var this_distance = Math.random() * (degrees_range - target_buffer) + target_buffer;
  var targ1_range = degrees_range - this_distance;
  var targ1_degs = Math.random() * targ1_range + degrees_min;
  var targ2_degs = targ1_degs + this_distance;

  if (Math.random() < 0.5)
    return [targ1_degs, targ2_degs];
  else
    return [targ2_degs, targ1_degs];
}

function sort_compare_numeric( a, b )
{
  return (a-b);
}

function window_was_resized()
{
  win_resize_trial_invalid = 1;
}

function set_object_center( obj_id, x_coord, y_coord )
// moves object center to (x_coord, y_coord)
//  in coordinate system where (0,0) is window center
// n.b.: can only call after globals win_center_x and win_center_y are set!
{
  var obj_width, obj_height;
  var top_coord, left_coord;

  obj_width = $(obj_id).outerWidth();
  obj_height = $(obj_id).outerHeight();
  top_coord = y_coord - (obj_height/2) + win_center_y;
  left_coord = x_coord - (obj_width/2) + win_center_x;
  $(obj_id).offset({ top: top_coord, left: left_coord});
}

function hide_targets()
{
  if (!targets_showing)
    return;

  var target_id;
  for (var i = 0; i < n_targets; i++)
  {
    target_id = target_id_prefix + i;
    $(target_id).hide();
  }

  targets_showing = 0;
}

function show_targets()
{
  if (targets_showing)
    return;

  var target_id;
  for (var i = 0; i < n_targets; i++)
  {
    target_id = target_id_prefix + i;
    $(target_id).show();
  }

  targets_showing = 1;
}

function hide_cursor()
{
  if (!cursor_showing)
    return;

  $('html').css({cursor: 'none'});
  cursor_showing = 0;
}

function show_cursor()
{
  if (cursor_showing)
    return;

  $('html').css({cursor: 'default'});
  cursor_showing = 1;
}

function lab4angle( ang )
{
  // a is x axis, b is y axis
  var a, b;
  var ang_r = deg2rad( ang );
  a = Math.cos( ang_r ) * lab_radius;
  b = Math.sin( ang_r ) * lab_radius;
  return [ lab_center[0], lab_center[1] + a, lab_center[2] + b ];
}

function deg2rad ( ang_d )
{
  return ang_d * (Math.PI / 180);
}

// based on Stefan's code, with minor tweak to add in iPad
function validate_browser( uas )
{
  var regex1 = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i;
  var regex2 = /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i;
  var r2_substr_length = 4;
  var uas_sub = uas.substr(0, r2_substr_length);

  if (regex1.test(uas))
    return 0;

  if (regex2.test(uas_sub))
    return 0;

  return 1;
}

function validate_worker_id( id_str )
{
  var min_length = 10;
  var max_length = 25;
  var bad_reg_ex = /\W/;

  if (id_str.length < min_length)
    return 0;

  if (id_str.length > max_length)
    return 0;

  if (id_str.charAt(0) != 'A' && id_str.charAt(0) != 'a')
    return 0;

  if (bad_reg_ex.test(id_str))
    return 0;

  return 1;
}

function check_worker_id_used_before( id_str )
{
  $.post( cgibin_dir + "vstm2d_check_workerid.py", { workerid: id_str }, check_worker_id_callback );
}

function check_worker_id_callback( check_status )
{
  if (check_status == "used")
    worker_id_used_before = 1;
  else if (check_status == "unused")
    worker_id_used_before = 0;
  else if (check_status == "error")
    worker_id_used_before = -2;
  else
    worker_id_used_before = -1;
}

// BELOW COURTESY OF GARY LUPYAN -- COPIED FROM
//  http://sapir.psych.wisc.edu/wiki/index.php/MTurk
function getParamFromURL( name )
{
  name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
  var regexS = "[\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

// BELOW BASED ON: http://www.easyrgb.com/index.php?X=MATH&H=08#text8
function lab2xyz( lab )
{
  var L = lab[0];
  var A = lab[1];
  var B = lab[2];

  var Y = (L+16)/116;
  var X = (A/500)+Y;
  var Z = Y - (B/200);

  if ((Y*Y*Y) > 0.008856)
    Y = Y*Y*Y;
  else
    Y = (Y - (16/116)) / 7.787;

  if ((X*X*X) > 0.008856)
    X = X*X*X;
  else
    X = (X - (16/116)) / 7.787;

  if ((Z*Z*Z) > 0.008856)
    Z = Z*Z*Z;
  else
    Z = (Z - (16/116)) / 7.787;

  X = X * 95.047;
  Y = Y * 100.000;
  Z = Z * 108.883;

  return [X,Y,Z];
}

// BELOW BASED ON: http://www.easyrgb.com/index.php?X=MATH&H=01#text1
function xyz2rgb( xyz )
{
  var X = xyz[0] / 100;
  var Y = xyz[1] / 100;
  var Z = xyz[2] / 100;

  var R = X *  3.2406 + Y * -1.5372 + Z * -0.4986
  var G = X * -0.9689 + Y *  1.8758 + Z *  0.0415
  var B = X *  0.0557 + Y * -0.2040 + Z *  1.0570

  if ( R > 0.0031308 )
    R = (1.055 * ( Math.pow(R, 1/2.4) )) - 0.055;
  else
    R = 12.92 * R;

  if ( G > 0.0031308 )
    G = (1.055 * ( Math.pow(G, 1/2.4) )) - 0.055;
  else
    G = 12.92 * G;

  if ( B > 0.0031308 )
    B = (1.055 * ( Math.pow(B, 1/2.4) )) - 0.055;
  else
    B = 12.92 * B;

  return [Math.round(R*255), Math.round(G*255), Math.round(B*255)];
}

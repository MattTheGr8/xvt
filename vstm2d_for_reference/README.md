**General to-do for new studies**


**If converting from a Yale study**

* Do all relevant tweaks to code (use vstm2d as a model)
* opendiff (aka FileMerge) can be your friend here
* syntax is `opendiff dir1 dir2` (or file1 file2, I think)


**After coding appears complete**

* check in to this Git repo
* on Zoidberg: do a `sudo git clone ... ` of the repo into a temp directory
* do a `sudo chmod 755 *.py` on all the Python scripts of the new experiment
* confirm that all other permissions are 644
* commit/push permission change to repo
* do a `sudo mv ...` of the new experiment's folder to /var/www/mturkstudi.es/public_html/active
* confirm that the permissions of the directory itself (should be 755), ownership (root for everything), and the files inside (should still be what they were a minute ago) are all OK
* to make Python scripts active, will need to edit /etc/apache2/sites-available/mturkstudi.es.conf
* experiment should now be active -- then it's on to the usual M-Turk checklist for making a new description, etc...



**OLD -- vstm2d to-do**

*programming side*

* Fix Python scripts to read/write files from data directories -- done
* Quick pass through HTML/CSS/Javascript to make sure it passes inspection and is up-to-date -- done, I think


*server side*

* Set up data directories outside web root -- done. /var/www/mturkstudi.es/data, group 'datareaders'
* Fix permissions inside script directories -- all Python exec no write (and no read?), all other stuff read-only -- done; has to be 755 (need read permission to execute, apparently
* Fix permissions in data directories to allow only writing by www-data... guess we have to let it read too -- done as much as possible
* Maybe write a little permissions configurator script if we need to... -- can set file permissions inside Python script for now; and for now, will just set stuff in public_html via Git (644 for everything except Python scripts, which will be 755)
* Upload/check in old Yale logs -- done, I think

*other stuff*

* Edit instructions text (both in PDF and in JavaScript) -- done, I think
* - do general pass-through Yale->UNL, make sure we comply with IRB here -- done, I think
* Load $$$
* TurkPrime account
* Maybe write snapshot backup cron job for data directory in case of catastrophe?

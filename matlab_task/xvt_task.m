function xvt_task(is_test)
Screen('Preference', 'SkipSyncTests', 1)

debug_mode      = false; %will run PTB in windowed mode so we can use GUI after dbstop if error
grayval         = 127;
resolution      = [1920 1080]; %x-resolution, y-resolution; must provide both

output_folder           = 'results';
output_file_format_main = fullfile(output_folder,'xvt_sub%.3d_run%.2d.mat');
output_file_format_prac = fullfile(output_folder,'xvt_sub%.3d_prac%.2d.mat');

n_trials_longprac = 60; % per run, evenly divisible by 3
n_trials_shortprac = 9;
n_trials = 90; % non-practice, evenly divisible by 3

stim_folder = 'stimuli';
image_types = {'.png' '.bmp'};
feedback_image_correct = 'smileyface.png';
feedback_image_incorrect = 'frownyface.png';
xhair3_file = 'xhair_3px.png';
xhair5_file = 'xhair_5px.png';

break_text          = 'Please take a break for as long as you like.\n\nWhen you are ready to begin the task again,\npress any key to resume.';
end_text            = 'That was the end of this run.\n\nPlease notify the experimenter.';

triggerkey = '*';

if nargin > 0
    if is_test
        Screen('Preference', 'SkipSyncTests', 1)
        %         debug_mode = true;
    end
end

%sizing
stim_size = [231 91]/2; % main stims
xhair5_size = [5 5];

% staircasing/timing parameters
allowed_distance = 50; % px; correct response must be within this distance from target
initial_trial_length = 1.5; % seconds
max_trial_length = 2;
min_trial_length = .500;
trial_adjust_correct = -.050;
trial_adjust_wrong   = .200;

% other timing parameters
initial_wait = 1; %in seconds
pretrial_fixation_dur = 1;
iti = 1;
flip_timewindow = .006;

% fixation point info
inner_fixation_color = [255 255 255]; %white
inner_fixation_radius = 4; %pixels
outer_fixation_radius = 5; %pixels
outer_fixation_color = [0 0 0];
% inner_fixation_color_iti = [127 127 127];

n_spaces_per_hemi = 180;
angle_offset = 2.5; %to avoid any stim appearing in cardinal directions
eccentricities = [325 425 525]; %in pixels
k_near = 1;
k_mid = 2;
k_far = 3;

%=================================
% determine if it's a practice run
%=================================
button = questdlg( 'Select run type','runtype','ShortPrac','LongPrac','Main','ShortPrac' );
short_practice = strcmp(button,'ShortPrac');
long_practice  = strcmp(button,'LongPrac');
main_run       = strcmp(button,'Main');

if short_practice
    conf_text_subinfo = 'You indicated that this is a short practice run.\n\nOutput will not be saved.';
elseif long_practice
    this_output_file_format = output_file_format_prac;
    conf_text_subinfo = 'You indicated that this is a long practice run.\n\nOutput will be saved in:\n\n%s';
elseif main_run
    this_output_file_format = output_file_format_main;
    conf_text_subinfo = 'You indicated that this is a main task run.\n\nOutput will be saved in:\n\n%s';
%     iti = iti+feedback_dur; %accounts for skipping feedback during non-practice runs
end

if ~short_practice
    subj_id = inputdlg('Enter a subject number:');
    subj_id = subj_id{1};
    sub_num = str2double(subj_id);
    outfile_exists = 1;
    run_num = 0;
    while outfile_exists
        run_num = run_num + 1;
        outfile_name = sprintf(this_output_file_format, sub_num, run_num);
        outfile_exists = exist(fullfile(pwd,outfile_name),'file');
    end
    waitfor(msgbox(sprintf(conf_text_subinfo,outfile_name)));
end

%=======================================================================
% determine if we are going to tweak difficulty via staircasing this run
%=======================================================================
if short_practice
    n_trials = n_trials_shortprac;
    trial_length = initial_trial_length;
elseif long_practice % staircasing
    n_trials = n_trials_longprac;
    trial_length = initial_trial_length;
    trial_length_final_weights = 1 - 1./exp(linspace(0,exp(1),n_trials+1)); %hard-coding this for now, may want to rethink later
    trial_length_final_weights = trial_length_final_weights ./ sum(trial_length_final_weights);
elseif main_run
    % looks for most recent practice run (probably 1, but will catch anything later)
    prac_check_block_num = 0;
    prac_check_exists = 1;
    while prac_check_exists
        prac_check_block_num = prac_check_block_num + 1;
        this_prac_check_name = sprintf(output_file_format_prac,sub_num,prac_check_block_num);
        prac_check_exists = exist(fullfile(pwd, this_prac_check_name), 'file');
    end
    infile_name = sprintf(output_file_format_prac, sub_num, prac_check_block_num-1);
    try
        lastrun_data = load(infile_name);
        trial_length = lastrun_data.trial_length_final;
        clear lastrun_data;
    catch
        error('Problem loading data from last run! Remember you need to do at least one staircased run before turning off difficulty adjustment... maybe that''s it?');
    end
    staircasing_text = 'Staircasing data found. Last trial length %.2fms. Is this OK?';
    staircasing_text = sprintf(staircasing_text,trial_length);    
    button = questdlg( staircasing_text,'runtype','Yes','Quit','Enter own data','Yes' );
    if strcmp(button,'Enter own data')
        tmp_trial_length = inputdlg('Enter trial length (s)');
        tmp_trial_length = tmp_trial_length{1};
        trial_length = str2double(tmp_trial_length);
    elseif strcmp(button,'Quit')
        error('User quit');
    end
    if trial_length < min_trial_length
        trial_length = min_trial_length;
    end
    if trial_length > max_trial_length
        trial_length = max_trial_length;
    end
end


%====================================
% set rand seed and init Psychtoolbox
%====================================
rng('shuffle');
magic_cleanup = onCleanup(@mj_ptb_cleanup);
[win, wRect] = mj_ptb_init(grayval, resolution, debug_mode); %#ok<ASGLU>

% ======================================
% load images, textures, etc
% ======================================
% makes pngs transparent
Screen(win,'BlendFunction',GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

% read in images and convert to textures
stim_list_fnames = mj_list_stim_dir(stim_folder,image_types);

stim_textures = pcl_load_image_tex_transparent(stim_list_fnames,win);

[~, feedback_corr_tex] = mj_load_image_texture(feedback_image_correct, grayval, win);
[~, feedback_incorr_tex] = mj_load_image_texture(feedback_image_incorrect, grayval, win);
[xhair3_dims, xhair3_tex] = mj_load_image_texture(xhair3_file, grayval, win);
[xhair5_dims, xhair5_tex] = mj_load_image_texture(xhair5_file, grayval, win);

%======================================
% rects and positioning
%======================================
center_point = [resolution(1)/2, resolution(2)/2];
inner_fixation_rect = mj_circle_rects_from_points(center_point(1), center_point(2), inner_fixation_radius);
outer_fixation_rect = mj_circle_rects_from_points(center_point(1), center_point(2), outer_fixation_radius);
img_rect = [0 0 stim_size];
xhair5_rect = [0 0 xhair5_size];

% master list of all possible locations
[x_left_near, x_right_near, y_near] = pcl_calc_item_coords(n_spaces_per_hemi, eccentricities(1), center_point, angle_offset);
[x_left_mid, x_right_mid, y_mid] = pcl_calc_item_coords(n_spaces_per_hemi, eccentricities(2), center_point, angle_offset);
[x_left_far, x_right_far, y_far] = pcl_calc_item_coords(n_spaces_per_hemi, eccentricities(3), center_point, angle_offset);

master_xs_near = [x_left_near ; x_right_near];
master_xs_mid  = [x_left_mid ; x_right_mid];
master_xs_far  = [x_left_far ; x_right_far];
master_ys_near = [y_near ; y_near];
master_ys_mid = [y_mid ; y_mid];
master_ys_far = [y_far ; y_far];

LR_cutoff = center_point(1);

% master_xs = [x_left_near x_right_near x_left_mid x_right_mid x_left_far x_right_far ];
% master_ys = [y_near y_near y_mid y_mid y_far y_far];
% master_distances = [ k_near*ones(size(x_left_near)) k_near*ones(size(x_right_near)) ...
%     k_mid*ones(size(x_left_mid)) k_mid*ones(size(x_right_mid))...
%     k_far*ones(size(x_left_far)) k_far*ones(size(x_right_far)) ]; % near=1 mid=2 far=3
% master_isleft = repmat( [true ; false], [numel(master_xs)/2 1]);
% master_xs = master_xs(:);
% master_ys = master_ys(:);
% master_distances = master_distances(:);
% master_isleft = master_isleft(:);

% ======================================
% define some general-purpose variables
% ======================================
n_stims = numel(stim_textures);
n_near = numel( [x_left_near x_right_near] );
n_mid = numel( [x_left_mid x_right_mid] );
n_far = numel( [x_left_far x_right_far] );

n_trials_per_distance = round(n_trials/3);
near_inds = Randi(n_near, [n_trials_per_distance,1]);
mid_inds  = Randi(n_mid, [n_trials_per_distance,1]);
far_inds  = Randi(n_far, [n_trials_per_distance,1]);

trials_x_near = master_xs_near(near_inds);
trials_y_near = master_ys_near(near_inds);
trials_x_mid = master_xs_mid(mid_inds);
trials_y_mid = master_ys_mid(mid_inds);
trials_x_far = master_xs_far(far_inds);
trials_y_far = master_ys_far(far_inds);

unshuffled_poslist_x = [ trials_x_near ; trials_x_mid ; trials_x_far];
unshuffled_poslist_y = [ trials_y_near ; trials_y_mid ; trials_y_far];

unshuffled_distances = [ k_near*ones(n_trials_per_distance,1) ; ...
    k_mid*ones(n_trials_per_distance,1) ; k_far*ones(n_trials_per_distance,1)];
unshuffled_isleft = [ trials_x_near<LR_cutoff ; ...
    trials_x_mid<LR_cutoff ; trials_x_far<LR_cutoff];

shuffle_inds = randperm(n_trials);

all_stimlist_inds = repmat( (1:n_stims)', [ceil(n_trials/n_stims), 1] );
all_stimlist_inds = all_stimlist_inds( randperm(numel(all_stimlist_inds)));
all_stimlist_inds = all_stimlist_inds( 1:n_trials );

all_stimlist_inds = all_stimlist_inds(shuffle_inds);
all_stimlist  = stim_list_fnames(all_stimlist_inds);
all_poslist_x = unshuffled_poslist_x(shuffle_inds);
all_poslist_y = unshuffled_poslist_y(shuffle_inds);
all_stim_distances = unshuffled_distances(shuffle_inds);
all_isleft    = unshuffled_isleft(shuffle_inds);

all_pretrial_onsets = nan(n_trials,1);
all_trial_onsets = nan(n_trials,1);
all_iti_onsets = nan(n_trials,1);

all_rt_times    = nan(n_trials,1);
all_rts         = nan(n_trials,1);

all_x_errs      = nan(n_trials,1);
all_y_errs      = nan(n_trials,1);
all_click_dists = nan(n_trials,1);
all_accs        = nan(n_trials,1);
all_trial_lengths = nan(n_trials,1);

%=====================================
% check refresh rate before continuing
%=====================================
flip_interval = Screen('GetFlipInterval',win);
refresh_rate = 1/flip_interval;
keypress = mj_prompt_key_code(win,[ 'Current refresh rate: about ' int2str(round(refresh_rate)) ' Hz\n'...
                                        'Press q to quit and change refresh rate,\n'...
                                        'any other key to begin']);
if strcmp(KbName(keypress), 'q') || strcmp(KbName(keypress), 'Q')
    error('User quit task');  
end

%===================================
% wait for trigger key to be pressed
%===================================
triggercode = KbName(triggerkey);
keypress = [];

while isempty(keypress) || ~isscalar(keypress) || keypress~=triggercode
    [keypress, start_time] = mj_prompt_key_code(win, 'Waiting to begin');
end

wait_till = start_time + initial_wait - flip_timewindow;

%===================================
% task starts here
%===================================

for i = 1:n_trials
        
    % draw fixation
%     Screen('FillOval', win, outer_fixation_color, outer_fixation_rect );
%     Screen('FillOval', win, inner_fixation_color, inner_fixation_rect );

    pretrial_fix_onset = Screen('Flip', win, wait_till);
    wait_till = pretrial_fix_onset + pretrial_fixation_dur - flip_timewindow;
    
    all_pretrial_onsets(i) = pretrial_fix_onset;
    
    ShowCursor('CrossHair');
    SetMouse( center_point(1), center_point(2), win );

    this_texture_ind = all_stimlist_inds(i);
    this_texture = stim_textures{this_texture_ind};
    this_x = all_poslist_x(i);
    this_y = all_poslist_y(i);

    target_rect = CenterRectOnPoint( img_rect, this_x, this_y );
    xhair5_rect  = CenterRectOnPoint( xhair5_rect, this_x, this_y);
    
    Screen('DrawTexture', win, this_texture, [], target_rect);
    Screen('DrawTexture', win, xhair5_tex, [], xhair5_rect);
    
    trial_onset = Screen('Flip', win, wait_till);
    wait_till = trial_onset + trial_length - flip_timewindow;
    all_trial_onsets(i) = trial_onset;
    
    % draw ITI fixation
    Screen('FillOval', win, outer_fixation_color, outer_fixation_rect );
    Screen('FillOval', win, inner_fixation_color, inner_fixation_rect );
    
    got_response = false;
    while GetSecs < wait_till
        %check mouse buttons
        [mouse_x, mouse_y, mouse_buttons] = GetMouse(win);
        if any(mouse_buttons)
            got_response = true;
            all_rt_times(i) = GetSecs;
            all_rts(i) = 1000*(all_rt_times(i) - trial_onset); % click times in ms
            click_x_err = mouse_x - this_x;
            click_y_err = mouse_y - this_y;
            
            click_dist  = sqrt(click_x_err^2 + click_y_err^2);
            all_x_errs(i) = click_x_err;
            all_y_errs(i) = click_y_err;
            all_click_dists(i) = click_dist;
            
            break;
        end
        WaitSecs(.001);
    end
        
    iti_onset = Screen('Flip', win, wait_till);
    HideCursor;
    wait_till = iti_onset + iti - flip_timewindow;
    
    all_iti_onsets(i) = iti_onset;
    all_trial_lengths(i) = trial_length;
    
    %=========================================================
    % do end-of-trial cleanup and some behavioral calculations
    % do staircasing, if selected earlier
    % staircasing: difficulty only adjusted if response was made
    %=========================================================
    if ~short_practice
        fprintf('\n\nTrial #: %d', i);
        fprintf('\nTrial length: %.3fs', trial_length);
        fprintf('\nClick distance: %.2f', all_click_dists(i));
        fprintf('\nTrial distance: %d\n', all_stim_distances(i));
    end
    
    all_accs(i) = all_click_dists(i) <= allowed_distance;
    if long_practice && ~(all_stim_distances(i)==k_near)
        if all_accs(i)==1
            trial_length = max(trial_length + trial_adjust_correct, min_trial_length);
        else % clicked too far away, or no response
            trial_length = min(trial_length + trial_adjust_wrong, max_trial_length);
        end
    end
    
end

mj_draw_text(win,end_text);

if ~short_practice
    if long_practice
        trial_length_final = sum(all_trial_lengths .* trial_length_final_weights');
        fprintf('\nFinal probe deviation weighted average: %f', trial_length_final );
    end
    clear stim_textures feedback_corr_tex feedback_incorr_tex xhair3_tex xhair5_tex
    clear x_left_far x_left_mid x_left_near x_right_far x_right_mid x_right_near
    clear y_far y_mid y_near
    clear master_xs_far master_xs_mid master_xs_near 
    clear master_ys_far master_ys_mid master_ys_near
    save(outfile_name);    
end

pct_correct = sum(all_accs, 'omitnan')/n_trials;
n_responses = sum(isfinite(all_accs));
fprintf('\nOverall accuracy: %d of %d (%.0f%%)\n',sum(all_accs,'omitnan'),n_trials,pct_correct*100);
fprintf('\nResponse rate: %d of %d (%.0f%%)\n',n_responses,n_trials,n_responses/n_trials*100);
fprintf('\nAverage click distance: %.2f\n',mean(all_click_dists(isfinite(all_click_dists))));

keyboard;

%-------------------------------------------------------------------------
function [w, wRect]=mj_ptb_init(grayval,new_resolution,debug_mode)

%do various initializations, pop up a window with the specified background
%grayscale color (0-255). Returns window pointer

% check for OpenGL compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Get screenNumber of stimulation display. We choose the display with
% the maximum index, which is usually the right one, e.g., the external
% display on a Laptop:
screens=Screen('Screens');
screenNumber=max(screens);

%try to set resolution
if nargin>1 && ~isempty(new_resolution)
    cur_res=Screen('Resolution',screenNumber);
    if cur_res.width ~= new_resolution(1) || cur_res.height ~= new_resolution(2)
        try
            Screen('Resolution',screenNumber,new_resolution(1),new_resolution(2));
        catch %#ok<CTCH>
            button=questdlg(['Unable to set resolution to ' int2str(new_resolution(1)) ' x ' int2str(new_resolution(2)) ...
                '. If eyetracking, fixation calculations will likely be inaccurate. Quit or continue anyway?'],'','Quit','Continue','Quit');
            if isempty(button) || strcmp(button,'Quit')
                error('Unable to set resolution; user quit');
            end
        end
    end
end

% Hide the mouse cursor:
if ~debug_mode
    HideCursor;
end

% Returns as default the mean gray value of screen:
grayval=GrayIndex(screenNumber,grayval/255); 

% Open a double buffered fullscreen window on the stimulation screen
% 'screenNumber' and choose/draw a gray background. 'w' is the handle
% used to direct all drawing commands to that window - the "Name" of
% the window. 'wRect' is a rectangle defining the size of the window.
% See "help PsychRects" for help on such rectangles and useful helper
% functions:
if debug_mode
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval,[50 50 new_resolution(1)+50 new_resolution(2)+50],[],[],[],[],[],kPsychGUIWindow);
else
    [w, wRect]=Screen('OpenWindow',screenNumber,grayval);
end

% Set text size (Most Screen functions must be called after
% opening an onscreen window, as they only take window handles 'w' as
% input:
Screen('TextSize', w, 28);

%don't echo keypresses to Matlab window
if ~debug_mode
    ListenChar(2);
end

% Do dummy calls to GetSecs, WaitSecs, KbCheck to make sure
% they are loaded and ready when we need them - without delays
% in the wrong moment:
KbCheck;
WaitSecs(0.1);
GetSecs;

% Set priority for script execution to realtime priority:
priorityLevel=MaxPriority(w);
Priority(priorityLevel);

if IsWin
    ShowHideWinTaskbarMex(0);
end

%-------------------------------------------------------------------------
function mj_ptb_cleanup

disp('Doing cleanup...');
Screen('CloseAll');
IOPort('CloseAll');
ShowCursor;
fclose('all');
Priority(0);
ListenChar(0);
if IsWin
    ShowHideWinTaskbarMex(1);
end

%-------------------------------------------------
function [code, secs]=mj_prompt_key_code(w,message)

mj_draw_text(w,message);
[secs, code]=KbWait([],2);
code=find(code);

%-------------------------------------------------
function mj_draw_text(w, message)

DrawFormattedText(w, message, 'center', 'center', WhiteIndex(w));
Screen('Flip', w);

%-------------------------------------------------
function file_list = mj_list_stim_dir( the_dir, file_types )

file_list = {};

for i = 1:length(file_types)
    file_pattern = fullfile(the_dir,['*' file_types{i}]);
    
    these_files = dir(file_pattern);
    if ~isempty(these_files)
        is_dirs = [these_files.isdir];
        these_files = {these_files.name};
        these_files = these_files(~is_dirs);
        valid_index = true(length(these_files), 1);
        for j = 1:length(these_files)
            if strcmp(these_files{j}(1), '.')
                valid_index(j) = false;
            end
        end
        these_files = these_files(valid_index);
        file_list = [file_list; these_files(:)]; %#ok<AGROW>
    end
end

for i = 1:length(file_list)
    file_list{i} = fullfile( the_dir, file_list{i} ); %#ok<AGROW>
end

% ------------------------------------------------------------------------
function image_data = pcl_load_image_file_transparent(file_list)
% reads list of PNGs and converts them to images with transparency info

image_data = cell(size(file_list));
for i=1:length(file_list)
    [tmp,~,alpha] = imread(file_list{i});
    tmp(:,:,4) = alpha;
    image_data{i} = tmp;
end

% ------------------------------------------------------------------------
function textures = pcl_load_image_tex_transparent(file_list,win)

image_data = pcl_load_image_file_transparent(file_list);

textures = cell(size(image_data));
for i=1:length(image_data)
    textures{i} = Screen('MakeTexture',win,image_data{i});
end

%---------------------------------------------------------------
function [dims, this_image]=mj_load_image_file(imagefile,grayval)

% if numel(grayval) > 1 || grayval >= 0
%     this_image=imread(imagefile,'backgroundcolor',grayval/255);
% else
%     this_image=imread(imagefile);
% end
this_image=imread(imagefile);
dims=size(this_image);

%-----------------------------------------------------------------
function [dims, texture]=mj_load_image_texture(imagefile,grayval,w)

[dims, this_image]=mj_load_image_file(imagefile,grayval);
texture=Screen('MakeTexture',w,this_image);

%-----------------------------------------------------------------
function rects=mj_circle_rects_from_points(xs, ys, radius)

rects=  [   xs(:)'-radius;
            ys(:)'-radius;
            xs(:)'+radius;
            ys(:)'+radius   ];

%-----------------------------------------------------------------
function [possible_item_xs_left, possible_item_xs_right, possible_item_ys] = ...
    pcl_calc_item_coords ( n_spaces_per_hemisphere, eccentricity, center_point, angle_offset)
%figure out coordinates for possible item locations
% locations will go from top towards bottom of screen

possible_item_angles_left  = linspace( pi/2, 3*pi/2, n_spaces_per_hemisphere+angle_offset );
possible_item_angles_right = linspace( pi/2,  -pi/2, n_spaces_per_hemisphere+angle_offset );
possible_item_angles_left  = possible_item_angles_left(2:(end-1));
possible_item_angles_right = possible_item_angles_right(2:(end-1));
possible_item_xs_left_raw  = cos( possible_item_angles_left  ) * eccentricity;
possible_item_xs_right_raw = cos( possible_item_angles_right ) * eccentricity;
possible_item_ys_raw       = sin( possible_item_angles_left  ) * eccentricity;
possible_item_xs_left      = possible_item_xs_left_raw  + center_point(1);
possible_item_xs_right     = possible_item_xs_right_raw + center_point(1);
possible_item_ys           = possible_item_ys_raw  + center_point(2);

possible_item_xs_left  = possible_item_xs_left(:);
possible_item_xs_right = possible_item_xs_right(:);
possible_item_ys       = possible_item_ys(:);

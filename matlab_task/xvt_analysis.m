function xvt_analysis

infolder = 'results';
infile_pattern = 'xvt_sub%.3d_run*';

subs = 1:4;
n_subs = numel(subs);

k_near = 1;
k_mid = 2;
k_far = 3;

% stim_pattern = 'stimuli/xvt_ang_%.3d.png';
stim_angle_inds = 17:19;

all_click_dists = [];
all_rts = [];
all_stimlist = {};
all_stim_distances = [];
all_stim_angles = [];
all_subnums = [];
all_poslist_x = [];
all_poslist_y = [];

error_cutoff = 150;

for i = 1:n_subs
    this_sub = subs(i);
    
    this_infile_pattern = sprintf(infile_pattern,this_sub);
    this_infile_pattern = fullfile(infolder,this_infile_pattern);
    all_runs = dir(this_infile_pattern);
    all_runs = {all_runs.name};
    all_runs = fullfile(infolder,all_runs);
    n_runs = numel(all_runs);
    
    sub_click_dists = [];
    sub_rts = [];
    sub_stimlist = {};
    sub_stim_distances = [];
    sub_stim_angles = [];
    sub_poslist_x = [];
    sub_poslist_y = [];

    for j = 1:n_runs
        this_infile = all_runs{j};
        data = load(this_infile);
        click_dists = data.all_click_dists;
        rts = data.all_rts;
        stimlist = data.all_stimlist;
        stim_distances = data.all_stim_distances;
        isleft = data.all_isleft;
        poslist_x = data.all_poslist_x;
        poslist_y = data.all_poslist_y;
        clear data
        
        n_trials = numel(rts);
        stim_angles = nan(n_trials,1);
        for k = 1:n_trials
            this_stim = stimlist{k};
            this_stim_ang = this_stim(stim_angle_inds);
            stim_angles(k) = str2double(this_stim_ang);
        end

        sub_click_dists = [sub_click_dists ; click_dists];
        sub_rts = [sub_rts ; rts ];
        sub_stimlist = [sub_stimlist ; stimlist];
        sub_stim_distances = [sub_stim_distances ; stim_distances];
        sub_stim_angles = [sub_stim_angles ; stim_angles ];
        sub_poslist_x = [ sub_poslist_x ; poslist_x ];
        sub_poslist_y = [ sub_poslist_y ; poslist_y ];
    end
        
    n_trials = numel(sub_rts);
    this_subnums = this_sub * ones(n_trials,1);
    
    all_subnums = [all_subnums ; this_subnums];
    all_click_dists = [all_click_dists ; sub_click_dists];
    all_rts = [all_rts ; sub_rts ];
    all_stimlist = [all_stimlist ; sub_stimlist];
    all_stim_distances = [all_stim_distances ; sub_stim_distances];
    all_stim_angles = [all_stim_angles ; sub_stim_angles ];
    all_poslist_x = [all_poslist_x ; sub_poslist_x];
    all_poslist_y = [all_poslist_y ; sub_poslist_y];
    
end

oops_inds = all_click_dists > error_cutoff;
all_click_dists(oops_inds) = nan;
all_rts(oops_inds) = nan;

far_click_dists = all_click_dists(all_stim_distances==k_far);
mid_click_dists = all_click_dists(all_stim_distances==k_mid);
near_click_dists = all_click_dists(all_stim_distances==k_near);

far_rts = all_rts(all_stim_distances==k_far);
mid_rts = all_rts(all_stim_distances==k_mid);
near_rts = all_rts(all_stim_distances==k_near);

far_stim_angles = all_stim_angles(all_stim_distances==k_far);
mid_stim_angles = all_stim_angles(all_stim_distances==k_mid);
near_stim_angles = all_stim_angles(all_stim_distances==k_near);

[all_theta, all_rho] = cart2pol(all_poslist_x,all_poslist_y);

stim_angles = unique(all_stim_angles);
n_stim_angles = numel(stim_angles);

stim_angle_dists = nan(n_subs,n_stim_angles,3); %near,mid,far
stim_angle_rts = nan(n_subs,n_stim_angles,3); %near,mid,far

for j = 1:n_subs
    
    sub_inds = all_subnums==j;
    
    for i = 1:n_stim_angles
        this_stim_angle = stim_angles(i);
        this_angle_inds = all_stim_angles==this_stim_angle;
        near_inds = all_stim_distances==k_near;
        mid_inds  = all_stim_distances==k_mid;
        far_inds  = all_stim_distances==k_far;
        
        stim_angle_dists(j,i,1) = nanmean(all_click_dists(this_angle_inds & near_inds & sub_inds));
        stim_angle_dists(j,i,2) = nanmean(all_click_dists(this_angle_inds & mid_inds & sub_inds));
        stim_angle_dists(j,i,3) = nanmean(all_click_dists(this_angle_inds & far_inds & sub_inds));
        
        stim_angle_rts(j,i,1) = nanmean(all_rts(this_angle_inds & near_inds & sub_inds));
        stim_angle_rts(j,i,2) = nanmean(all_rts(this_angle_inds & mid_inds & sub_inds));
        stim_angle_rts(j,i,3) = nanmean(all_rts(this_angle_inds & far_inds & sub_inds));
    end
end

n_trials = numel(all_theta);

[~,~,resid1] = regress(all_click_dists,[-all_theta ones(n_trials,1)]);
[~,~,resid2] = regress(resid1,[-all_rho ones(n_trials,1)]);
[~,~,resid3] = regress(resid2,[-all_rts ones(n_trials,1)]);

stim_angle_dists_resids = nan(n_subs,n_stim_angles);

long_inds = all_stim_angles==stim_angles(2) | all_stim_angles==stim_angles(3) | all_stim_angles==stim_angles(4);
short_inds = all_stim_angles==stim_angles(12) | all_stim_angles==stim_angles(13) | all_stim_angles==stim_angles(14);

sub1_long = resid3(long_inds & all_subnums==1);
sub1_short = resid3(short_inds & all_subnums==1);
sub2_long = resid3(long_inds & all_subnums==2);
sub2_short = resid3(short_inds & all_subnums==2);
sub3_long = resid3(long_inds & all_subnums==3);
sub3_short = resid3(short_inds & all_subnums==3);
sub4_long = resid3(long_inds & all_subnums==4);
sub4_short = resid3(short_inds & all_subnums==4);


resid3 = all_click_dists;
sub1_long = resid3(long_inds & all_subnums==1);
sub1_short = resid3(short_inds & all_subnums==1);
sub2_long = resid3(long_inds & all_subnums==2);
sub2_short = resid3(short_inds & all_subnums==2);
sub3_long = resid3(long_inds & all_subnums==3);
sub3_short = resid3(short_inds & all_subnums==3);
sub4_long = resid3(long_inds & all_subnums==4);
sub4_short = resid3(short_inds & all_subnums==4);

all_sub_trials = [sub1_long sub1_short sub2_long sub2_short sub3_long sub3_short sub4_long sub4_short]; 

for j = 1:n_subs
    
    sub_inds = all_subnums==j;
    
    for i = 1:n_stim_angles
        this_stim_angle = stim_angles(i);
        this_angle_inds = all_stim_angles==this_stim_angle;
        
        stim_angle_dists_resids(j,i) = nanmean(resid3(this_angle_inds & sub_inds));
        
    end
end



which_stims = [2 3 4 12 13 14];
which_stims = 1:15;

figure;

subplot(n_subs,1,1)
bar(squeeze(stim_angle_dists_resids(1,which_stims)))
subplot(n_subs,1,2)
bar(squeeze(stim_angle_dists_resids(2,which_stims)))
subplot(n_subs,1,3)
bar(squeeze(stim_angle_dists_resids(3,which_stims)))
subplot(n_subs,1,4)
bar(squeeze(stim_angle_dists_resids(4,which_stims)))


figure;

subplot(n_subs,1,1)
bar(squeeze(stim_angle_dists(1,which_stims,1)))
subplot(n_subs,1,2)
bar(squeeze(stim_angle_dists(2,which_stims,1)))
subplot(n_subs,1,3)
bar(squeeze(stim_angle_dists(3,which_stims,1)))
subplot(n_subs,1,4)
bar(squeeze(stim_angle_dists(4,which_stims,1)))

figure;
subplot(n_subs,1,1)
bar(squeeze(stim_angle_dists(1,which_stims,2)))
subplot(n_subs,1,2)
bar(squeeze(stim_angle_dists(2,which_stims,2)))
subplot(n_subs,1,3)
bar(squeeze(stim_angle_dists(3,which_stims,2)))
subplot(n_subs,1,4)
bar(squeeze(stim_angle_dists(4,which_stims,2)))

figure;
subplot(n_subs,1,1)
bar(squeeze(stim_angle_dists(1,which_stims,3)))
subplot(n_subs,1,2)
bar(squeeze(stim_angle_dists(2,which_stims,3)))
subplot(n_subs,1,3)
bar(squeeze(stim_angle_dists(3,which_stims,3)))
subplot(n_subs,1,4)
bar(squeeze(stim_angle_dists(4,which_stims,3)))

keyboard;


















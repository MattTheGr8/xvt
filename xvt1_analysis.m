function xvt1_analysis

infolder = 'xvt1_first_twelve_batches_working/seems_good';
infile_pattern = 'xvt1_trial_log_A*txt';

owd = pwd;
cd(infolder);
subs = dir(infile_pattern);
subs = {subs.name};
subs = fullfile(pwd,subs);
n_subs = numel(subs);
cd(owd);

all_eccen =  [];
all_targn =  [];
% all_clickx = [];
% all_clicky = [];
% all_targx =  [];
% all_targy =  [];
all_error_dists = [];

all_accs = [];
all_accs_targn = [];
all_rts = [];

for i = 1:n_subs
    this_sub = subs{i};
    fid = fopen(this_sub,'rt');
    fields = textscan(fid,'%s%d%d%d%d%f%d%d%d%f%f%f%d%d%d%s','delimiter','\t');
    fclose(fid);
    
    workerid =      fields{1}; %#ok<NASGU>
    trialnum =      fields{2}; %#ok<NASGU>
    trialnum_c =    fields{3}; %#ok<NASGU>
    acc_feedback =  fields{4};
    resp_time =     fields{5};
    eccen =         fields{6};
    targn =         fields{7};
    click_x =       fields{8};
    click_y =       fields{9};
    targ_deg =      fields{10}; %#ok<NASGU>
    targ_x =        fields{11};
    targ_y =        fields{12};
    old_targtime =  fields{13}; %#ok<NASGU>
    new_targtime =  fields{14}; %#ok<NASGU>
    win_resized =   fields{15}; %#ok<NASGU>
    timestamp_str = fields{16}; %#ok<NASGU>
    
    acc_feedback =  logical(acc_feedback);
    if sum(acc_feedback) ~= 150
        disp(this_sub);
        error('weird number of correct trials?');
    end
    
    all_accs =      [all_accs; acc_feedback]; %#ok<AGROW>
    all_accs_targn= [all_accs_targn; double(targn)]; %#ok<AGROW>
    
    eccen =         eccen(acc_feedback);
    targn =         targn(acc_feedback);
    click_x =       click_x(acc_feedback);
    click_y =       click_y(acc_feedback);
    targ_x =        targ_x(acc_feedback);
    targ_y =        targ_y(acc_feedback);
    resp_time =     resp_time(acc_feedback);
    
    eccen =         eccen(31:end);
    targn =         targn(31:end);
    click_x =       click_x(31:end);
    click_y =       click_y(31:end);
    targ_x =        targ_x(31:end);
    targ_y =        targ_y(31:end);
    resp_time =     resp_time(31:end);
    
    click_x =       double(click_x);
    click_y =       double(click_y);
    
    err_dists =     sqrt((click_x - targ_x).^2 + (click_y - targ_y).^2);
    
    all_eccen =  [all_eccen; eccen]; %#ok<AGROW>
    all_targn =  [all_targn; targn]; %#ok<AGROW>
    all_error_dists = [all_error_dists; err_dists]; %#ok<AGROW>
    all_rts   =  [all_rts; resp_time];
%     all_clickx = [all_clickx; click_x]; %#ok<AGROW>
%     all_clicky = [all_clicky; click_y]; %#ok<AGROW>
%     all_targx =  [all_targx; targ_x]; %#ok<AGROW>
%     all_targy =  [all_targy; targ_y]; %#ok<AGROW>
end

eccen_bins  = 100:40:300;
n_ecc_bins  = numel(eccen_bins) - 1;
uniq_targns = sort(unique(all_targn));
n_targns    = numel(uniq_targns);
mean_dists  = nan(n_targns,1);
std_errs    = mean_dists;
ecc_dists   = nan(n_targns, n_ecc_bins);

degs = (uniq_targns*10)+20;

% all_error_dists = all_error_dists(all_rts<900);
% all_targn = all_targn(all_rts<900);

for i = 1:n_targns
    these_dists = all_error_dists(all_targn==uniq_targns(i));
    these_eccs  = all_eccen(all_targn==uniq_targns(i));
    mean_dists(i) = mean(these_dists);
    std_errs(i)   = std(these_dists)/sqrt(numel(these_dists));
    
    for j = 1:n_ecc_bins
        ecc_dists(i,j) = mean(these_dists(these_eccs>eccen_bins(j) & these_eccs<eccen_bins(j+1)));
    end
end

close all;
figure;
% hold on;
% plot(degs,mean_dists,'-','linewidth',1);
errorbar(degs,mean_dists,std_errs,std_errs,'linewidth',2);
set(gca,'fontsize',14);
set(gca,'fontweight','bold');
% set(gca,'ytick',11:16); %first 18
% set(gca,'ytick',8:12);  %next  22
% set(gca,'ytick',10:13); ylim([9.5 13.5]); %first 40 combined
set(gca,'ytick',11:13); ylim([11 13.5]); %first 84
% set(gca,'ytick',14:17); ylim([13.5 17.5]); %first 84, only RTs < 900msec

figure;
% [scat_degs,scat_eccs] = meshgrid( degs, eccen_bins(1:end-1)+20 );
% scatter( scat_degs(:), scat_eccs(:), ecc_dists(:) * 50 );
hold on;
for j = 1:n_ecc_bins
    plot(degs,ecc_dists(:,j) - mean(ecc_dists(:,j)) + 10*j,'linewidth',2 );
end
xlim([0 180]);
set(gca,'fontsize',14);
set(gca,'fontweight','bold');
set(gca,'ytick',10:10:50);
set(gca,'yticklabel',{'low ecc' ' ' ' ' ' ' 'high ecc'});
ylim([0 60]);

for i = 1:15
    acc_pcts(i) = sum( all_accs(all_accs_targn==(i-1))) / sum(all_accs_targn==(i-1)) ; %#ok<AGROW,NASGU>
end
figure;
plot(degs,acc_pcts,'linewidth',2);
set(gca,'fontsize',14);
set(gca,'fontweight','bold');
% set(gca,'ytick',10:13); ylim([9.5 13.5]); %first 40 combined
xlim([10 170])
ylim([.73 .83])


keyboard;
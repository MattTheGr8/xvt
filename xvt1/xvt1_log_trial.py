#!/usr/bin/python

output_fname = '/var/www/mturkstudi.es/data/active/xvt1_trial_log_{0}.txt' #the placeholder will get filled with the worker ID
owner_name = 'www-data'
group_name = 'datareaders'
python_log_dir = '/var/www/mturkstudi.es/log/pylogs'

import sys, cgi, cgitb, datetime
import os, pwd, grp, stat

cgitb.enable(display=0, logdir=python_log_dir)

formdata = cgi.FieldStorage()

workerid     = formdata.getvalue("workerid",      "WORKERID_NULL")
trialnum     = formdata.getvalue("trialnum",      "TRIAL_NULL")
trialnum_c   = formdata.getvalue("trialnum_c",    "TRIAL_C_NULL" )
acc_feedback = formdata.getvalue("acc_feedback",  "ACC_FB_NULL")
resp_time    = formdata.getvalue("resp_time",     "RESP_TIME_NULL")
eccen        = formdata.getvalue("eccen",         "ECCEN_NULL")
targn        = formdata.getvalue("targn",         "TARG_NUM_NULL")
click_x      = formdata.getvalue("click_x",       "CLICK_X_NULL")
click_y      = formdata.getvalue("click_y",       "CLICK_Y_NULL")
targ_deg     = formdata.getvalue("targ_deg",      "TARG_DEG_NULL")
targ_x       = formdata.getvalue("targ_x",        "TARG_X_NULL")
targ_y       = formdata.getvalue("targ_y",        "TARG_Y_NULL")
old_targtime = formdata.getvalue("old_targtime",  "OLD_TARGTIME_NULL")
new_targtime = formdata.getvalue("new_targtime",  "NEW_TARGTIME_NULL")
win_resized  = formdata.getvalue("win_resized",   "WIN_RESIZE_NULL")

output_fname = output_fname.format(workerid);
timestamp_str = '{0}'.format( datetime.datetime.utcnow() )
filehandle = open(output_fname, 'a')

to_print = '{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\n'.format(
           workerid, trialnum, trialnum_c, acc_feedback, resp_time, eccen, targn, click_x, click_y,
           targ_deg, targ_x, targ_y, old_targtime, new_targtime, win_resized, timestamp_str )
filehandle.write( to_print )
filehandle.close()

#new -- try fixing permissions/ownership up real nice
uid = pwd.getpwnam(owner_name).pw_uid
gid = grp.getgrnam(group_name).gr_gid
os.chown(output_fname, uid, gid)
os.chmod(output_fname, stat.S_IWUSR | stat.S_IRUSR | stat.S_IRGRP )

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')
sys.stdout.write('Done.')

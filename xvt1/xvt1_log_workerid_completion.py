#!/usr/bin/python

worker_db_fname = '/var/www/mturkstudi.es/data/active/xvt1_completed_worker_log_unl.txt'
# file format: tab-delimited text
# fields: worker id, timestamp of completion, completion code
lockfile_fname  = '/var/www/mturkstudi.es/data/active/xvt1_completed_worker_log_lockfile' #doing this the ghetto way for now
owner_name = 'www-data'
group_name = 'datareaders'
python_log_dir = '/var/www/mturkstudi.es/log/pylogs'

import sys, cgi, cgitb, datetime, os.path, time
import os, pwd, grp, stat

cgitb.enable(display=0, logdir=python_log_dir)

formdata = cgi.FieldStorage()

workerid     = formdata.getvalue("workerid",      "WORKERID_NULL")
comp_code    = formdata.getvalue("comp_code",     "COMPCODE_NULL")

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')

timestamp_str = '{0}'.format( datetime.datetime.utcnow() )
line_to_print = '{0}\t{1}\t{2}\n'.format( workerid, timestamp_str, comp_code )

#check lock status
try_count = 0
lockfile_exists = os.path.isfile( lockfile_fname )
while lockfile_exists:
    time.sleep(0.2)
    try_count += 1
    if try_count > 10:
        sys.stdout.write('error')
        sys.exit()
    lockfile_exists = os.path.isfile( lockfile_fname )

open(lockfile_fname, 'a').close() #creates lockfile and immediately closes it

try:
    filehandle = open(worker_db_fname, 'a')
    filehandle.write(line_to_print)
    filehandle.close()
    
    #new -- try fixing permissions/ownership up real nice
    uid = pwd.getpwnam(owner_name).pw_uid
    gid = grp.getgrnam(group_name).gr_gid
    os.chown(worker_db_fname, uid, gid)
    os.chmod(worker_db_fname, stat.S_IWUSR | stat.S_IRUSR | stat.S_IRGRP )
    
    os.remove(lockfile_fname)
except:
    sys.stdout.write('error')
    os.remove(lockfile_fname)
    filehandle.close()
    sys.exit()

if (workerid == "WORKERID_NULL") or (workerid == ""):
    sys.stdout.write('error')
else:
    sys.stdout.write('success')

#!/usr/bin/python

worker_db_fnames = ['/var/www/mturkstudi.es/data/active/xvt1_completed_worker_log_unl.txt',
                   ]
# file format: tab-delimited text
# fields: worker id, timestamp of completion, completion code
python_log_dir = '/var/www/mturkstudi.es/log/pylogs'

import sys, cgi, cgitb, os.path

cgitb.enable(display=0, logdir=python_log_dir)

formdata = cgi.FieldStorage()

workerid     = formdata.getvalue("workerid",      "WORKERID_NULL")

sys.stdout.write('Content-type: text/plain; charset=UTF-8\n\n')
if (workerid == "WORKERID_NULL") or (workerid == ""):
    sys.stdout.write('error')
    sys.exit()

for current_filename in worker_db_fnames:
    if (not os.path.isfile( current_filename )):
        continue

    try:
        filehandle = open(current_filename, 'r')
        all_lines = filehandle.readlines()
        filehandle.close()
    except:
        sys.stdout.write('error')
        sys.exit()

    for this_line in all_lines:
        this_line = this_line.strip( '\n' )
        this_line_cells = this_line.split( '\t' )
        this_workerid = this_line_cells[0]
        if (workerid == this_workerid):
            sys.stdout.write('used')
            sys.exit()

sys.stdout.write('unused')
